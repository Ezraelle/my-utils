# My utils

Etre capable de:

    • Avoir deux listes “physiques” (représenté séparément sur une page), une “A faire” et une “Finie”.
    • Ajouter une tâche (qui se met automatiquement dans la liste “A faire”).
    • Supprimer une tâche (quelque soit la liste).
    • Marquer une tâche comme “finie”, cela déplace la tâche de “A faire” à “Finie”

